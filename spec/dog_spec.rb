require 'spec_helper'
require_relative '../dog'

describe Dog do
  describe '#bark' do
    subject { described_class.new.bark }

    it { is_expected.to eq('barks') }
  end
end
